# Установка  Docker.

*Установка и настройка Docker для Linux Ubuntu*

**Удаление старых версий**

  Чтобы удалить предыдущие версии docker, docker.io, или docker-engine, существует команда : 

    sudo apt-get remove docker docker-engine docker.io containerd runc 


 Все данные, такие как контейнеры, сети, образы сохранятся. Если нужно удалить и их тоже, используйте команды : 

    sudo rm -rf /var/lib/docker

    sudo rm -rf /var/lib/containerd

 **Установка через репозиторий**

 1. Обновите индекс пакета apt:

    sudo apt-get update

    sudo apt-get install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release


2. Добавьте официальный GPG-ключ Docker'а:

    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg


3. Используйте следующую команду для настройки стабильного репозитория:

    echo \
    "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null


4. Обновите индекс пакета apt и установите последнюю версию Docker Engine и containerd:

    sudo apt-get update

    sudo apt-get install docker-ce docker-ce-cli containerd.io


5. Убедитесь, что Docker Engine установлен правильно:

    sudo docker


*Установка и настройка Docker для Linux Ubuntu*

**Установка Docker Desctop**

1. Скачайте установщик Docker. Для этого перейдите по ссылке ниже и нажмите на кнопку Docker Desktop for Windows:

    https://docs.docker.com/desktop/windows/install/


2. Перезагрузите ПК и войдите в Bios. В биосе найдите пункт "Аппаратная виртуализация" и включите его.


3. Следуя инструкциям установщика, установите и настройте Docker.