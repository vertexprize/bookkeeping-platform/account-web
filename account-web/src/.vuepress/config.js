const { description } = require('../../package')

module.exports = {
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#title
   */
  title: 'Bookkeeping Platform',
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#description
   */
  description: description,

  /**
   * Extra tags to be injected to the page HTML `<head>`
   *
   * ref：https://v1.vuepress.vuejs.org/config/#head
   */
  head: [
    ['meta', { name: 'theme-color', content: '#3eaf7c' }],
    ['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
    ['meta', { name: 'apple-mobile-web-app-status-bar-style', content: 'black' }]
  ],

  /**
   * Theme configuration, here is the default theme configuration for VuePress.
   *
   * ref：https://v1.vuepress.vuejs.org/theme/default-theme-config.html
   */
  themeConfig: {
    repo: '',
    editLinks: false,
    docsDir: '',
    editLinkText: '',
    lastUpdated: false,
    nav: [
      {
        text: 'Оглавление',
        link: '/documentation/'
      },
      {
        text: 'Быстрый старт',
        link: '/getting-started/'
      },
      {
        text: 'Проект на сайте Gitlab',
        link: 'https://gitlab.com/vertexprize'
      },
      {
        text: 'Документация',
        link: '/docks-for-users/'
      },
    ],
    sidebar: {
      '/documentation/': [
        {
          title: 'Архитектура',
          collapsable: true,
          children: [
            'architecture',
            'iotdevice',
          ]
        },
        {
          title: 'Документация',
          collapsable: true,
          children: [
            'docker-installation',
            'portainer-installation',
            'quarkus-start',
            'terms-dictionary',
            'architecture',
          ]
        },

      ],
      '/modules/':[        
        {
          title: 'Описание модулей',
          collapsable: true,          
        },
      ],
      

      '/docks-for-users/':[
      
        
        {
          title: 'Установка и настройка Portainer',
          collapsable: true,
          children: [
            'portainer-installation'
          ]
        },
      
      ],

    }
  },

  /**
   * Apply plugins，ref：https://v1.vuepress.vuejs.org/zh/plugin/
   */
  plugins: [
    '@vuepress/plugin-back-to-top',
    '@vuepress/plugin-medium-zoom',
    'vuepress-plugin-mermaidjs',
  ]
}
