# Терминология бухгалтерии.

Счет | Account  
План счетов | Chart of accounts  
Баланс счета (остаток на счете) | Account balance   
Субсчет | Sub-account  
Транзакция | Transaction  
Главная книга | General ledger  
Двойная запись | Double-entry   
Пробный баланс | Trial balance   
Описание (транзакции) | Description    
Дата | Date      
Финансовая отчетность | Financial statements     
Бухгалтерский баланс | Balance sheet     
Отчет о прибылях и убытках | Income statement        
Отчет о движении денежных средств | Cash flow statement     
Операционные расходы | Operating expenses        
Дивиденд | Dividend    
Стандарты бухгалтерского учета | Accounting Standards    
Отчетный период | Accounting Period    
Сумма количество (в (дол./тыс. руб)) | Amount (amount in ($))
