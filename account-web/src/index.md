---
home: true
heroImage: https://v1.vuepress.vuejs.org/hero.png
tagline: 
actionText: Документация →
actionLink: /documentation/
features:
- title:   Полноценный бухгалтерский функционал
  details: Создание и запись счетов, сохранение проводок, получение отчетов и статистики
- title:   Свободное распространение
  details: Доступ для любого человека 
- title:   Современная архитектура
  details: Основана на микросервисах, БД PostgreSQL

footer:   vertexprize.org   2023
---
