# Установка и настройка Ubuntu в систему Windows

Для того чтобы работать с терминалом Ubuntu в Windows нужно совершить несколько шагов:

1. Активация WSL(Windows subsystem for Linux). Сделать это можно при помощи графического интерфейса :  Панель управления → Программы и компоненты → Включение и отключение компонентов Windows, установить галочку напротив пункта Подсистема Windows для Linux и применить изменения нажатием на кнопку ОК.

2. Перезагрузка ПК.

3. В Windows Store необходимо найти и установить дистрибутив Ubuntu.

Теперь вам доступен терминал Linux в системе Windows. Запуск можно производить из меню «Пуск» или из командной строки. Для вызова Linux команды напрямую можно воспользоваться следующим синтаксисом в CMD: bash -c "команда Linux". Но удобнее всё таки запускать Linux через меню «Пуск».