#  Программная архитектура

В разделе приведены сведения об архитектуре программного обеспечения платформы EducationSocketSystem. Для примера была взята система работы с объектом Person.
Рассматривается внутренняя организация следущих классов:

- esCore(EducationSystemCore);
- PersonMicroservice;
- Hibernate;
- HumanResourcesMicroservice;
- PersonMicroservice;
- PersonWebCabinet;
- DeanOfficeWebCabinet;
- PersonWebCabinet;


##  esCore(EducationSystemCore)

В esCore располагаются все сущности, с которыми База Данных будет взаимодействовать - адреса, люди, кафедры и т.п. Это ядро системы в которое в будущем будут добавляться новые сущности и организовываться их связь друг с другом.



## PersonWebCabinet

PersonWebCabinet предоставляет пользователю графический интерфейс для работы с базой данных в браузере. Пользователь может зарегестрироваться на сайте, изменить данные профиля, просмотреть актуальную информацию об институте.


## PersonMicroservice

PersonMicroservice отвечает за обработку интернет запросов на размещение, изменение и удаление данных из базы, их называют HTTP-запросами. В нём присутствуют два класса - PersonController и PersonService. Контроллер принимает к себе HTTP-запросы(Put,Get,Post,Delete), интерпретирует их и передаёт в Person Service, который их обрабатывает и передаёт далее в Hibernate.

## Hibernate
Система упрощает взаимодействие с базами данных, а именно позволяет не использовать в программном коде язык SQL для манипуляции с таблицами с базами данных. Работа с таблицами заменяется работой с объектами, такая технология носит название Object Relational Mapper(ORM).

## HumanResourcesMicroservice;

Микросервис отдела кадров.


## DeanOfficeWebCabinet

DeanOfficeWebCabinet - Web-кабинет работника деканата. Предоставляет пользователю возможность редактировать базу данных пользуясь браузером.
  
```mermaid
sequenceDiagram
    PersonWebCabinet->>PersonMicroservice:Запрос
    PersonMicroservice->>Panache:Запрос
   Hibernate-->>PersonMicroservice:Ответ
    PersonMicroservice-->>PersonWebCabinet:Ответ
```
<p align="center"> Рис. 1. Работа пограммы на примере Person. </p>