# Создание проекта Person Microservice

**Создание person-Service**

    Person-Service – класс, который предоставляет команды для работы с объектом базы данных Person.
К объявлению класса применена аннотация @ApplicationScoped, которая значит, что bean-компонент находится в области приложения. Эта аннотация относится к Quarkus.

    Далее объявлен метода getAll и в его объявлении указан Массив класса Person, с которым метод getAll ведёт работу. Этот метод возвращает все сохранённые объекты класса Person.

    После getAll создан метод saveStudent использующий объект класса Person. В saveStudent для сохранения студента используется метод persistAndFlush() который нужен для

    За этим идёт метод deletePerson использующий объект класса Person. Для удаления используется метод delete().
Перед каждым из методов объявлена аннотация @Transactional. Она принадлежит Spring-фреймворку, и определяет область действия одной транзакции БД. Транзакция БД происходит внутри области действий persistence context.
Транзакция в Java – серия действий, которые должны быть успешно совершены. Если одно или несколько действий потерпели неудачу, все последующие действия будут отменены и программа останется неизменной.

**Создание person-Controller**

    Перед классом указывается аннотация @Path(“/person”), которая указывает путь, по которому будет совершён запрос.
    
    Внутри класса первым делом с помощью аннотации @Inject внедряется объект класса PersonService. Это нужно для того, чтобы пользоваться методами класса PersonService в PersonController и создаём объект personService класса PersonService. Теперь объект personService имеет все классы PersonService.

    Далее создаётся объект Logger, для того, чтобы в дальнейшем мы могли создавать логи при выполнении методов.

    После мы делаем метод getAllPersons() в объявлении которого указываем массив объектов Person под названием person. Так как мы делаем метод для получения объекта из базы данных нужно использовать HTTP команду Get. Для этого, перед методом указываем аннотацию @GET. Так же создаём аннотацию @Produces(MediaType.APPLICATION_JSON) для того, чтобы полученный в дальнейшем список был в формате JSON. Внутри метода создаём логи для того, чтобы видеть что происходит с БД в момент выполнения команды и удачно или нет она была выполнена. Далее созданному в getAllPersons() массиву persons присваиваем значения метода personService.getAll() и в качестве возвращаемого значения указываем массив persons. 