    Для создания проекта необходимо создать папку, где проект будет располагаться, затем - перейти в эту папку в терминале. Находясь в терминале, необходимо ввести следующую команду:

     npm create <название проекта>
     npm create quickstart

    осле создания папки для проекта, Vue предложит выбрать версию для установки - vue2/vue3. Отличие заключается в том, что vue3 быстрее и легче, а также поддерживает язык Type Script.
Поэтому, необходимо выбрать vue3. Установив все пакеты, нужно перейти в созданную папку в терминале и запустить сервер разработки командой ‘npm run serve’. После этого надо установить дополнительные файлы командами:

     npm install primevue@^3.11.1 --save
     npm install primeicons --save

    Теперь есть рабочая заготовка сайта, на которую можно зайти и редактировать внешний вид в браузере, изменяя код в файлах проекта. Для работы лучше всего подходит Visual Studio Code. 
Чтобы открыть проект в Studio Code, необходимо в терминале зайти в папку проекта и написать 'code .'. 
Открыв проект, в папке src необходимо найти main.js и вставить в него следующий код:

         import { createApp } from 'vue'
         import App from './App.vue'
         import PrimeVue from 'primevue/config';
         import Button from 'primevue/button';
         import 'primevue/resources/themes/saga-blue/theme.css'      //theme
         import 'primevue/resources/primevue.min.css'               //core css
         import 'primeicons/primeicons.css' 
         import InputSwitch from 'primevue/inputswitch';
         const app = createApp(App);

         app.use(PrimeVue);
         app.component('Button',Button);
         app.component('InputSwitch',InputSwitch);
         app.mount('#app')


         const app = createApp(App) //* при помощи метода createApp был сделан экземпляр приложения.*/


    Если нужно задействовать какой-либо компонент в проекте, этот компонент нужно импортировать. Импорты делаются в ‘Main.js’, например:

         import Button from 'primevue/button

    Далее, нужно из импорта добавить компонент в проект 'app.component('Button',Button);'. В кавычках может быть указано любое слово, но лучшим вариантом будет указывать название, которое присутствует в импорте.

    Внешний вид проект настраивается в файле ‘App.vue’. Здесь можно добавить картинку, установить стили и цвета текста настроить работу компонентов. 

         <template>
            <Button label="Подключить" />
            <InputSwitch v-model="checked" />
         </template>

         <script>
            export default {
                name: 'App',
                components: {
                }
                }
         </script>

         <style>
          #app {
          font-family: Avenir, Helvetica, Arial, sans-serif;
          -webkit-font-smoothing: antialiased;
          -moz-osx-font-smoothing: grayscale;
          text-align: center;
          color: #2c3e50;
          margin-top: 60px;
          }
         </style>


    <template> - прорисовка Web-страницы

    <script> - здесь пишется код Java Script
    export - дополнительные настройки, например, данные для Web-страницы
    <style> - стиль текста

    Чтобы добавить импортированный элемент ‘Button’ на страницу, нужно в разделе <template> написать строку <Button label="Надпись" />. Добавленным компонентам нужно задать стиль. Это делается путём добавления в 'Main.js’ в раздел ‘Import’ следующих строк :

         import 'primevue/resources/themes/saga-blue/theme.css'      //theme
         import 'primevue/resources/primevue.min.css'               //core css
         import 'primeicons/primeicons.css'