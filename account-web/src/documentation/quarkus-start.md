# Создание микросервиса Quarkus.

**Процесс создания микросервиса на фреймфорке Quarkus:**

1. На сайте <a href="https://quarkus.io/">quarkus.io</a> перейти в раздел <a href="https://code.quarkus.io/">START CODING</a>.

2. При переходе в раздел откроется страница с информацией о проекте и перечнем доступных для подключения к проекту технологий.
В блоке «Configure your application» заполняются поля названия группы, проекта, инструмента сборки и версий проекта и технологий. 

**В нашем случае использованы следующие параметры:**

    Group — org.vertexprize.bookkeeping 
    Artifact — account-server
    Build Tool — Maven     
    Version — 0.0.1 
    Java Version — 11

**Далее необходимо выбрать технологии, которые будут использоваться в проекте:**

    а) RESTEasy Reactive (в разделе «Web»)
    б) Hibernate ORM (в разделе «Data»)
    в) JDBC Driver — PostgreSQL (так же в разделе «Data»)

При выборе технологий они добавляются в проект, который автоматически собирается после нажатия кнопки «Generate youre application» и становится доступным для скачивания в формате ZIP файла. Файл скачивается и распаковывается в папку проекта Bookkeeping Platform.

**3. Микросервис запускается из папки проекта «account-server» через терминал командой:**
       
    ./mvnw compile quarkus:dev 


Перед тестовым запуском необходимо скомпилировать проект в IDE и заполнить файл «application.properties». В процессе сборки система загрузит на ПК все необходимые для работы технологии. 

**В файл «application.properties» прописываются следующие параметры:**


    #Файл настройки микросервиса счетов
    quarkus.http.port=9797
    quarkus.http.host=0.0.0.0
    quarkus.http.cors=true
    quarkus.http.cors.origins=*
    quarkus.http.cors.methods=GET,PUT,POST,DELETE
    quarkus.http.cors.access-control-max-age=24H
    quarkus.http.cors.access-control-allow-credentials=true

    #Настройка подключения к базе данных Posgress
    quarkus.datasource.db-kind = postgresql
    quarkus.datasource.username = postgres
    quarkus.datasource.password = postgres
    quarkus.datasource.jdbc.url = jdbc:postgresql://localhost:5432/postgres


    #Режим работы с базой данных

    #drop and create the database at startup (use `update` to only update the schema drop-and-create)
    quarkus.hibernate-orm.database.generation = drop-and-create
    #quarkus.hibernate-orm.database.generation = none
    quarkus.hibernate-orm.packages = org.vertexprize.bookkeeping.account

