# Установка  Portainer.

*Примечание : Установка приложения Portainer возможна только после установки и настройки приложения Docker.

*Установка и настройка Portainer для Linux Ubuntu*

1. На официальном сайте https://docs.portainer.io нажмите вкладку Install.


2. Выберите вкладку "Community Edition" и нажмите кнопку "INSTALL PORTAINER CE".


3. Нажмите "Set up a new Portainer Server installation".


4. Нажмите "Docker Standalone".


5. Нажмите "Install Portainer with Docker on Linux".


6. Создайте том, который Portainer Server будет использовать для хранения своей базы данных:

   docker volume create portainer_data


7. Скачайте и установите контейнер Portainer Server:

   sudo docker run -d -p 8000:8000 -p 9443:9443 --name portainer --restart=always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer-ce:2.11.1

Portainer будет располагаться в браузере по адресу localhost:9000.




*Установка и настройка Portainer для Windows 10*

1. На официальном сайте https://docs.portainer.io нажмите вкладку Install.


2. Выберите вкладку "Community Edition" и нажмите кнопку "INSTALL PORTAINER CE".


3. Нажмите "Set up a new Portainer Server installation".


4. Нажмите "Docker Standalone".


5. Нажмите "Install Portainer with Docker on WSL / Docker Desktop".


6. Создайте том, который Portainer Server будет использовать для хранения своей базы данных:

   docker volume create portainer_data


7. Скачайте и установите контейнер Portainer Server:

   docker run -d -p 8000:8000 -p 9443:9443 --name portainer --restart=always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data     portainer/portainer-ce:2.11.1

Теперь, когда установка завершена, вы можете войти в свой экземпляр Portainer Server, открыв веб-браузер и перейдя по адресу localhost:9443