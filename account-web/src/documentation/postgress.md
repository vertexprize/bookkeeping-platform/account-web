**Установка и подключение базы данных Postgres в Docker**

1. Зайти на сайт Docker Hub :

     https://hub.docker.com/search?q=

2. В строке поиска написать Postgres. В открывшемся списке найти postgres с подписью "official image".

3. В соответствии с инструкциями на странице открыть терминал и ввести: 

     sudo docker pull postgres

4. В терминале ввести: 

      docker run --name hiik-postgres -p 6432:5432 -e POSTGRES_PASSWORD=postgres -d postgres:14.2

*Примечание:* 
    sudo docker run         - Запуск Докера;
    --name                  - Дать имя программе; 
    -p                      - (Port) Задать номер порта;
    -e POSTGRES_PASSWORD    - (Enviroment) Устанавливает имя, пароль и название контейнера;
    -d                      - (dockerimage) Версия постгресса.


5. Открыть среду Netbeans и в окне выбора проекта, перейти на вкладку "Services".

6. Правой клавишой мыши нажать на "Databases" и выбрать "New connection".

7. В поле "Driver" выбрать "PostgresQL" и нажать на кнопку "Next".

8. В окне "Port" указать тот порт, который давали базе данных в терминале, пароль задать postgres. Нажать "Test Connection"

9. Если высветилось сообщение "Connection succeed", нажать на кнопку "Next". Если высветилось сообщение об ошибке - тщательно перепроверить порт, и задать правильные настройки.



Чтобы нашу базу данных увидел Quarkus, необходимо зайти в pom.xml quarkus-проекта и в зависимостях добавить необходимые драйверы :

     <!-- Hibernate ORM specific dependencies -->
<dependency>
    <groupId>io.quarkus</groupId>
    <artifactId>quarkus-hibernate-orm-panache</artifactId>
</dependency>

<!-- JDBC driver dependencies -->
<dependency>
    <groupId>io.quarkus</groupId>
    <artifactId>quarkus-jdbc-postgresql</artifactId>
</dependency>

Далее необходимо добавить настройки в файл application.properties quarkus-проекта :

# настройка подключения к базе данных Postgres
quarkus.datasource.db-kind = postgresql
quarkus.datasource.username = postgres
quarkus.datasource.password = postgres
quarkus.datasource.jdbc.url = jdbc:postgresql://localhost:2432/mydatabase

# режим работы с базой данных
quarkus.hibernate-orm.database.generation = drop-and-create

10. В терминале зайти в файл с quarkus-приложением, и запустить его командой:

     ./mvnw compile quarkus:dev

Программа должна выдать информацию о том, что quarkus подключен к postgres. 

Чтобы пользоваться инструментами postgres в коде, необходимо все классы расширить с помошью класса PanacheEntity.